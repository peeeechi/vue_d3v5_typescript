import Vue from 'vue'
import MyArea from './myArea';

const App = new Vue({
    el: '#app',
    template : 
    `
    <div class="app">
        <my-area message="mes1" message2="mes2"></my-area>
    </div>
    `,

    components: {
        'my-area': MyArea
    }
})