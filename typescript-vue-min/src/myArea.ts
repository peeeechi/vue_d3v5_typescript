import Vue from 'vue';
import * as d3 from 'd3';
import Component from 'vue-class-component';

@Component({
    template: `
    <div>
        <h2>{{message}}</h2>
        <h2>{{message2}}</h2>
        <p>{{count}}</p>
        <p>
            <button @click="method_1">My Method 1</button>
        </p>
        <p>
            <button @click="method_2">My Method 2</button>
        </p>
        <div id="area"></div>
    </div>
    `,
    props:[
        'message',
        'message2'
    ],
})

export default class MyArea extends Vue {
    count:number = 0;

    method_1():void{
        this.count = this.count + 1;
    }

    method_2():void{
        // 1. データの準備
  var dataset = [
    [5, 20],
    [480, 90],
    [250, 50],
    [100, 33],
    [330, 95],
    [410, 12],
    [475, 44],
    [25, 67],
    [85, 21],
    [220, 88]
  ];
 
  var width = 400; // グラフの幅
  var height = 300; // グラフの高さ
  var margin = { "top": 30, "bottom": 60, "right": 30, "left": 60 };
 
  // 2. SVG領域の設定
  var svg = d3.select("#area").append("svg").attr("width", width).attr("height", height);
 
  // 3. 軸スケールの設定
  var xScale = d3.scaleLinear()
    .domain([0, d3.max(dataset, function(d) { return d[0]; })])
    .range([margin.left, width - margin.right]);
 
  var yScale = d3.scaleLinear()
    .domain([0, d3.max(dataset, function(d) { return d[1]; })])
    .range([height - margin.bottom, margin.top]);
 
  // 4. 軸の表示
  var axisx = d3.axisBottom(xScale).ticks(5);
  var axisy = d3.axisLeft(yScale).ticks(5);
 
  svg.append("g")
    .attr("transform", "translate(" + 0 + "," + (height - margin.bottom) + ")")
    .call(axisx)
    .append("text")
    .attr("fill", "black")
    .attr("x", (width - margin.left - margin.right) / 2 + margin.left)
    .attr("y", 35)
    .attr("text-anchor", "middle")
    .attr("font-size", "10pt")
    .attr("font-weight", "bold")
    .text("X Label");
 
  svg.append("g")
    .attr("transform", "translate(" + margin.left + "," + 0 + ")")
    .call(axisy)
    .append("text")
    .attr("fill", "black")
    .attr("x", -(height - margin.top - margin.bottom) / 2 - margin.top)
    .attr("y", -35)
    .attr("transform", "rotate(-90)")
    .attr("text-anchor", "middle")
    .attr("font-weight", "bold")
    .attr("font-size", "10pt")
    .text("Y Label");
 
  // 5. プロットの表示
  svg.append("g")
    .selectAll("circle")
    .data(dataset)
    .enter()
    .append("circle")
    .attr("cx", function(d) { return xScale(d[0]); })
    .attr("cy", function(d) { return yScale(d[1]); })
    .attr("fill", "steelblue")
    .attr("r", 4);
    }
}